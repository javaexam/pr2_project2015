package it.unica.pr2.progetto2015.g45783_g45678_g46388;

public class Semplice implements it.unica.pr2.progetto2015.interfacce.SheetFunction
{

	public Object execute(Object... a) 
	{ //da modificare con is number ecc ecc
		try{		
		Integer j = (Integer)a[0];
		return j;
		}catch(Exception e){System.out.println("non casta a intero");return 0;}
	}

	public final String getCategory() 
	{
		return "Informazione";
	}

	public final String getHelp()
	{
		return "Prende in ingresso un parametro e restituisce un numero se viene passato un numero, 0 se viene passata una stringa";
	}

	public final String getName() 
	{
		return "NUM";
	}
	
}
