package it.unica.pr2.progetto2015.g45783_g45678_g46388;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.zip.GZIPInputStream;
import java.io.InputStream;
import java.nio.file.Paths;
import java.nio.file.Files;

public class Custom implements it.unica.pr2.progetto2015.interfacce.SheetFunction
{
	public Object execute(Object... a)
	{
		String res=download((String)a[0]);
		return res;	
	}
	
	public String download(String elemento) //il nome va cambiato in qualcosa tipo cerca
	{
		String path="https://torrentz.eu/search?q="+elemento;
		path = path.replace(' ','+'); //sostituisco gli spazi con i +
		return recuperaPrimoLink(path);
	}
	
	private String recuperaPrimoLink(String percorso) //recupero il primo link
	{
		try{
			Document doc = Jsoup.connect(percorso).userAgent("Mozilla").timeout(30000).get();
			Elements results = doc.select(".results"); //classe results
			Elements dt = results.select("dt"); //tag dt
			Elements firstLink = dt.select("a[href]"); //mi serve solo il primo della lista OK

			String link = firstLink.attr("abs:href");
			return cercaKatProxy(link);
		}catch(Exception e){ System.out.println("eccezione: "+e); }
		return "error";
	}

	private String cercaKatProxy(String qui)
	{
		try{
			Document doc = Jsoup.connect(qui).userAgent("Mozilla").timeout(30000).get();
			Elements divDownload = doc.select(".download");
			Elements dt = divDownload.select("dt"); //seleziono tutti i dt che avranno al loro interno i link a href
			Elements links = dt.select("a[href]");
			for (Element tuttiLink : links)
			{	//devo recuperare la testa (cio che c'è tra // e /)
				
				String sitoDiOrigine = tuttiLink.attr("abs:href").split("/")[2]; //mi restituirà 1337x.to, katproxy.com ecc ecc
				if(sitoDiOrigine.equals("kat.cr"))
				{	//se è uguale l'ho trovato e restituisco il link di katproxy.com
					//System.out.println(tuttiLink.attr("abs:href")); //stavo cercando lui
					return downloadTorrent(tuttiLink.attr("abs:href"));
				}
			}
			
		}catch(Exception e){ System.out.println("eccezione: "+e); }
		return "Errore katProxy non trovato, possibile cambio di dominio?";
	}

	private String downloadTorrent(String page)
	{
		try{
			Document doc = Jsoup.connect(page).userAgent("Mozilla").timeout(30000).get();
			Elements link = doc.select("a[class=siteButton giantButton    verifTorrentButton]"); //dovrebbe esserci solo questo

			String torrentLink = link.attr("abs:href");
			return torrentLink;
		}catch(Exception e){ System.out.println("eccezione: "+e); }
		return "error";
	}

	public final String getCategory() 
	{
		return "File";
	}

	public final String getHelp()
	{
		return "Prende in ingresso una chiave di ricerca e restituisce il link al relativo torrent";
	}

	public final String getName() 
	{
		return "TorrentDownload";
	}




}