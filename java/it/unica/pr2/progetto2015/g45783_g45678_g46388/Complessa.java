package it.unica.pr2.progetto2015.g45783_g45678_g46388;

import java.util.*;

public class Complessa implements it.unica.pr2.progetto2015.interfacce.SheetFunction 
{
	public int converti(Object s)
	{
		String str=(String)s;
		int value=0;
		byte x=0;
		for (int i=str.length()-1; i>=0; i--)
		{
		switch (str.charAt(i))
		{
				case 'I':
				case 'i':
					if (x<=0)	value+=1;
					else		value-=1;
					x=0;
					break;
				case 'V':
				case 'v':
					if (x<=1)	value+=5;
					else		value-=5;
					x=1;
					break;
				case 'X':
				case 'x':
					if (x<=2)	value+=10;
					else		value-=10;
					x=2;
					break;
				case 'L': 
				case 'l':
					if (x<=3)	value+=50;
					else		value-=50;
					x=3;
					break;
				case 'C':
				case 'c':
					if (x<=4)	value+=100;
					else		value-=100;
					x=4;
					break;
				case 'D':
				case 'd':
					if (x<=5)	value+=500;
					else		value-=500;
					x=5;
					break;
				case 'M':
				case 'm':
					if (x<=6)	value+=1000;
					else		value-=1000;
					x=6;
					break;
			}
		}
		return value;
	}	

	public final Object execute(Object... args)
	{
		return (converti(args[0]));
	}

	public final String getCategory() 
	{
		return "Testo";
	}

	public final String getHelp()
	{
		return "Prende in ingresso un numero romano (String) e lo converte nella sua controparte araba (int)";
	}

	public final String getName() 
	{
		return "ARABO";
	}	
}	