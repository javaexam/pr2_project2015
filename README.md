# README #

Progetto2015:

Consiste nell'utilizzo di classi java all'interno dei fogli di calcolo di libreoffice tramite l'utilizzo di obba.
All'interno del foglio di calcolo vi saranno tre schede descritte di seguito:

# Semplice:

Riproduce una funzione nativa di libreoffice ritenuta di complessità semplice, in questo caso la scelta è ricaduta sulla funzione NUM() che opera nei seguenti modi:
NUM(n) restituisce n dove n è un numero, NUM(stringa) restituisce 0, NUM(VERO) restituisce 1, NUM(FALSO) restituisce 0.

# Complessa

Anche qui è stata riprodotta una funzione nativa di libreoffice di complessità leggermente superiore, ovvero ARABO().
Tale funzione si occupa di prendere in ingresso un numero romano e ne restituisce il suo corrispettivo valore secondo la notazione araba appunto:
ARABO(iii) restituisce 3

# Custom
Per la parte custom è stata creata una funzione che utilizza una chiave di ricerca per trovare i file .torrent inerenti quella determinata chiave, appoggiandosi ad un aggregatore online e restituendone il link per il download in formato .torrent appunto

# ****ATTENZIONE**: l'utilizzo del protocollo torrent per il trasferimento dei file non è illegale, ma lo scaricamento di contenuti illeciti e/o coperti da copyright viola le vigenti normative in materia di anti-pirateria. Ne io ne il mio gruppo ne siamo responsabili, la nostra funzione permette solo la ricerca di contenuti in un formato lecito, la chiave di ricerca deve essere scelta CONSAPEVOLMENTE da voi.**

A causa dell'illegalità di alcuni contenuti trasmessi tramite il suddetto protocollo, durante l'utilizzo della funzione custom e in presenza di determinate parole chiave si può ottenere come risultato la frase: "Errore katProxy non trovato, possibile cambio di dominio?" che sta ad indicare semplicemente che la chiave inserita non è presente (per motivi non legati a noi) all'interno dei server utilizzati dall'aggregatore.
